import face_recognition					# for face comparisons
import os								# for file handling
dirs = 'C:\\Images\\'				# path to parent directory
flag = False
for i in os.listdir(dirs):			# reading folders in parent direcotry
	path = os.path.join(dirs,i)
	#print(i)
	for j in os.listdir(path):			# reading file one-by-one in folders
		print(j)

		unknown_img = face_recognition.load_image_file('modi.jpg')				# loading test image
		known_img = face_recognition.load_image_file((os.path.join(path,j)))	# loading saved image

		unknown_encoding = face_recognition.face_encodings(unknown_img)[0]		# encoding test image
		known_encoding = face_recognition.face_encodings(known_img)[0]			# encoding saved image

		result = face_recognition.compare_faces([known_encoding],unknown_encoding)		# comparing bothe the images

		if result == [True]:		# if match found
			name = i 				# getting name of the folder
			flag = True				# changing flag to true
			break					# breaking out of inner loop
	if flag:
		print("Match found in",name)
		break 							# breaking out of outer for loop after match is found 
	
if not flag:							# if no match found
	print('No Match found')